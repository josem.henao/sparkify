import glob
import os

import pandas as pd
import psycopg2

from sql_queries import *


def process_song_file(cur, filepath):
    """
    Reads the JSON info in the filepath and loads the info in the tables songs and artists
    :param cur: The cursor to acces the DB
    :param filepath: The absolute local path to the file to process
    :return: None
    """
    # open song file
    df = pd.read_json(filepath, lines=True)
    # insert song record
    song_data = list(df.loc[:, ['song_id', 'title', 'artist_id', 'year', 'duration']].values[0])

    cur.execute(song_table_insert, song_data)

    # insert artist record
    artist_data = df.loc[:, ['artist_id', 'artist_name', 'artist_location', 'artist_latitude', 'artist_longitude']]
    artist_data.loc[artist_data['artist_latitude'].isna(), 'artist_latitude'] = 0
    artist_data.loc[artist_data['artist_longitude'].isna(), 'artist_longitude'] = 0
    artist_data = list(artist_data.values[0])

    cur.execute(artist_table_insert, artist_data)


def process_log_file(cur, filepath):
    # open log file
    df = pd.read_json(filepath, lines=True)

    # filter by NextSong action
    df = df.loc[df.loc[:, 'page'] == 'NextSong',]

    # convert timestamp column to datetime
    t = df.loc[:, 'ts'].apply(pd.to_datetime)

    # insert time data records
    time_data = time_data = {
        'timestamp': df.loc[:, 'ts'].values,
        'hour': t.dt.hour.values,
        'day': t.dt.day.values,
        'weekofyear': t.dt.weekofyear.values,
        'month': t.dt.month.values,
        'year': t.dt.year.values,
        'dayofweek': t.dt.dayofweek.values
    }
    # column_labels = ['timestamp', 'hour', 'day', 'weekofyear', 'month', 'year', 'dayofweek']
    time_df = time_df = pd.DataFrame(time_data)

    for i, row in time_df.iterrows():
        cur.execute(time_table_insert, list(row))

    # load user table
    user_df = df[['userId', 'firstName', 'lastName', 'gender', 'level']]
    user_df = user_df.drop_duplicates()

    # insert user records
    for i, row in user_df.iterrows():
        cur.execute(user_table_insert, row)

    # insert songplay records
    for index, row in df.iterrows():
        # get songid and artistid from song and artist tables
        cur.execute(song_select, (row.song, row.artist, row.length))
        results = cur.fetchone()

        if results:
            songid, artistid = results
        else:
            songid, artistid = None, None

        # insert songplay record
        songplay_data = (
            pd.to_datetime(row.registration), row.userId, row.level, songid, artistid, row.sessionId, row.location,
            row.userAgent)
        cur.execute(songplay_table_insert, songplay_data)


def process_data(cur, conn, filepath, func):
    # get all files matching extension from directory
    all_files = []
    for root, dirs, files in os.walk(filepath):
        files = glob.glob(os.path.join(root, '*.json'))
        for f in files:
            all_files.append(os.path.abspath(f))

    all_files = list(filter(lambda x: '.ipynb_checkpoints' not in x, all_files))
    # get total number of files found
    num_files = len(all_files)

    print('{} files found in {}'.format(num_files, filepath))

    # iterate over files and process
    for i, datafile in enumerate(all_files, 1):
        func(cur, datafile)
        conn.commit()
        print('{}/{} files processed.'.format(i, num_files))


def main():
    conn = psycopg2.connect("host=127.0.0.1 dbname=sparkifydb user=student password=student")
    cur = conn.cursor()

    process_data(cur, conn, filepath='data/song_data', func=process_song_file)
    process_data(cur, conn, filepath='data/log_data', func=process_log_file)

    conn.close()


if __name__ == "__main__":
    main()
