# Sparkify

This work is part of the projects proposed in the Udacity Data Engineering Nanodegree. 
It consists of creating an ETL to store the data of an online music platform. 
Basically two types of data are generated. The songs with information such as artist, 
location and publication date. The interactions of the users of the platform with these songs. 
In this second part of the information, the data of the users, the songs they listen to, and the time they spend on the
platform are recorded, as well as other data.

_**This repo only contains the final `version of the code ran in the Udacity and was not run in a local environment**_

## How to run

In order to see the results of this ETL you have to run two scripts. The `create_tables.py` will run some SQL 
commands defined in the `sql_queries.py`. These SQL scripts will drop tables in order to create new ones with the most 
recent version of the SQL commands. The `etl.py` Contains all the process to ETL (Extract Transform and Load) the files
in the `/data` folder. In the terminal type in order the following commands:
1. `python create_tables.py`
2. `python etl.py`
  
If you are not in a virtual environment and want to run with a specific Python version you 
have to indicate it tiping `python3` or `python3.8`.

## The Data Model

Since The intention of the stakeholders is to trace the interaction of the users over the songs in the Sparkify platform
We need to create a start schema using a fact table (songplays`) and some other 4 dimension tables to support the 
analytics (`users`, `artists`, `songs`, `time`). The following tables were considered to store the information needed:

1. `sonplays` table: Contains the data regarding user's interactions with the songs. Contains the following columns:
>- songplay_id
>- start_time 
>- user_id    
>- level      
>- song_id    
>- artist_id  
>- session_id 
>- location   
>- user_agent  

2. `users` table: Contains the info about the user in the Sparkify platform. The following columns are used:
>- user_id   
>- first_name
>- last_name 
>- gender    
>- level
3. `artists` table: This table contains information about the artists of the songs. The columns are:
>- artist_id
>- name     
>- location 
>- latitude 
>- longitude
4. `songs` table: Contains the info about the songs: The columns used are the following:
>- song_id  
>- title    
>- artist_id
>- year     
>- duration 
5. `time` table: This table is used to decompose the timestamp of an interaction. the decomposition is all the columns 
   in the table except start_time:
>- start_time 
>- hour
>- day
>- week
>- month     
>- year      
>- weekday